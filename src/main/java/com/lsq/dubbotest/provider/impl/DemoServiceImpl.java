package com.lsq.dubbotest.provider.impl;

import java.util.ArrayList;
import java.util.List;

import com.lsq.dubbotest.provider.DemoService;

/**   
* @Title: DemoServiceImpl.java 
* @Package com.lsq.dubbotest.provider.impl 
* @Description: TODO(用一句话描述该文件做什么) 
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月16日 上午10:33:35 
* @version V1.0   
*/
public class DemoServiceImpl implements DemoService {

    @Override
    public String sayHello(String name) {
        return "Hello " + name;
    }

    @Override
    public List getUsers() {
        List list = new ArrayList();
        User u1 = new User();
        u1.setName("hejingyuan");
        u1.setAge(20);
        u1.setSex("f");

        User u2 = new User();
        u2.setName("xvshu");
        u2.setAge(21);
        u2.setSex("m");

        
        list.add(u1);
        list.add(u2);
        
        return list;
    }
}

