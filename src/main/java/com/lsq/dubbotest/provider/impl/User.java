package com.lsq.dubbotest.provider.impl;

import java.io.Serializable;

/**   
* @Title: User.java 
* @Package com.lsq.dubbotest.provider.impl 
* @Description: TODO(用一句话描述该文件做什么) 
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月16日 上午10:43:34 
* @version V1.0   
*/
public class User implements Serializable{
        /** 
    * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
    */ 
    private static final long serialVersionUID = 6578572936948288973L;
        private String name;
        private Integer age;
        private String sex;
        
        public String getName() {
            return name;
        }
        public Integer getAge() {
            return age;
        }
        public String getSex() {
            return sex;
        }
        public void setName(String name) {
            this.name = name;
        }
        public void setAge(Integer age) {
            this.age = age;
        }
        public void setSex(String sex) {
            this.sex = sex;
        }
}

