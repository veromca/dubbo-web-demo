package com.lsq.dubbotest.provider.impl;
import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;
/**   
* @Title: Provider.java 
* @Package com.lsq.dubbotest.provider.impl 
* @Description: 提供端
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月16日 上午10:55:04 
* @version V1.0   
*/
public class Provider {

    public static void main(String[] args) throws IOException {
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.start();
        System.in.read(); // 为保证服务一直开着，利用输入流的阻塞来模拟

    }

}

