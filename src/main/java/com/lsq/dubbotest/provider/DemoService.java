package com.lsq.dubbotest.provider;

import java.util.List;

/**   
* @Title: DemoService.java 
* @Package com.lsq.dubbotest.provider 
* @Description: TODO(用一句话描述该文件做什么) 
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月16日 上午10:31:16 
* @version V1.0   
*/
public interface DemoService {
    String sayHello(String name);

    public List getUsers();

}

