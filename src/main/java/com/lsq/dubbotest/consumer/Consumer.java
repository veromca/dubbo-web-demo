package com.lsq.dubbotest.consumer;

import java.io.IOException;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lsq.dubbotest.provider.DemoService;

/**   
* @Title: Consumer.java 
* @Package com.lsq.dubbotest.consumer 
* @Description: 消费端
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月16日 上午11:24:09 
* @version V1.0   
*/
public class Consumer {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[] { "applicationContextConsumer.xml" });
        context.start();

        DemoService demoService = (DemoService) context.getBean("demoService");
        String hello = demoService.sayHello("liusongqing");
        System.out.println(hello);

        List list = demoService.getUsers();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
        System.in.read();

    }

}

